package com.indizen.bigdata.kafka.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.Arrays;
import java.util.Properties;

public class AtMostOnceConsumer {

    private final static String topic = "";
    private final static String bootstrap = "";
    private final static String consumerGroup = "";

    public static void main(String[] str) throws InterruptedException {
        System.out.println("Starting  AtMostOnceConsumer ...");
        execute();
    }

    private static void execute() throws InterruptedException {
        KafkaConsumer<String, String> consumer = createConsumer();

        // TODO
        // Subscribe to all partition in that topic.
        // 'assign' could be used here instead of 'subscribe' to subscribe to specific partition.

        processRecords(consumer);
    }

    private static KafkaConsumer<String, String> createConsumer() {
        Properties props = new Properties();
        // TODO
        // Bootstrap and group properties

        // TODO
        // Set this property, if auto commit should happen.

        // TODO
        // Auto commit interval, kafka would commit offset at this interval.

        // TODO
        // How to control number of records being read in each poll

        // TODO
        // Set the if you want to always read from beginning.

        props.put("key.deserializer", "");
        props.put("value.deserializer", "");

        return new KafkaConsumer<String, String>(props);
    }

    private static void processRecords(KafkaConsumer<String, String> consumer) {
        while (true) {
            // TODO
            // Polling
            ConsumerRecords<String, String> records = null;

            long lastOffset = 0;

            for (ConsumerRecord<String, String> record : records) {
                // TODO
                // Get key && value

                lastOffset = record.offset();
            }

            System.out.println("lastOffset read: " + lastOffset);

            try {
                process();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // TODO
            // Control the offset commit.
            // Do this call after you finish processing the business process.

        }

    }

    private static void process() throws InterruptedException {
        // create some delay to simulate processing of the message.
        Thread.sleep(20);
    }
}
