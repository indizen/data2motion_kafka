package com.indizen.bigdata.kafka.consumer;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.util.Collections;
import java.util.Properties;

public class MyConsumer {

    private final static String TOPIC = "";
    private final static String TOPIC_PARTITIONED = "";
    private final static String BOOTSTRAP_SERVERS = "";

    private static Consumer<Integer, String> createConsumer() {
        final Properties props = new Properties();

        //TODO create consumer
        return null;
    }


    static void runConsumer() throws InterruptedException {
        final Consumer<Integer, String> consumer = createConsumer();
        //TODO: Subscribe

        final int maxAttempts = 100;
        int attempts = 0;

        while (true) {
            //TODO: Polling
            final ConsumerRecords<Integer, String> consumerRecords = null;
            if (consumerRecords.isEmpty()) {
                attempts++;
                if (attempts > maxAttempts) break;
                else continue;
            }
            printOutput(consumerRecords);
        }

        //TODO: close consumer

        System.out.println("DONE");
    }


    static void printOutput(ConsumerRecords<Integer, String> consumerRecords) {
        consumerRecords.forEach(record -> {
            // TODO
            System.out.printf("Consumer Record:(key: %d, value: %s, partition: %d, offset: %d)\n", "", "", "", "");
        });
    }

    static void runConsumerPartitioned() throws InterruptedException {
        final Consumer<Integer, String> consumer = createConsumer();
        //TODO: Subscribe

        final int maxAttempts = 100;
        int attempts = 0;

        while (true) {
            //TODO: Polling
            final ConsumerRecords<Integer, String> consumerRecords = null;
            if (consumerRecords.isEmpty()) {
                attempts++;
                if (attempts > maxAttempts) break;
                else continue;
            }
            printOutput(consumerRecords);

        }

        //TODO: close consumer

        System.out.println("DONE");
    }
}
