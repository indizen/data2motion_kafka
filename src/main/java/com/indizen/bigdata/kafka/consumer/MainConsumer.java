package com.indizen.bigdata.kafka.consumer;

import static com.indizen.bigdata.kafka.consumer.MyConsumer.runConsumer;

public class MainConsumer {

    public static void main(String... args) throws Exception {
        runConsumer();
        // runConsumerPartitioned();
    }
}
