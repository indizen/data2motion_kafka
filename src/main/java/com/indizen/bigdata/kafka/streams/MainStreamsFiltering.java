package com.indizen.bigdata.kafka.streams;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class MainStreamsFiltering {

    private final static String BOOTSTRAP_SERVERS = "";
    private final static String input_topic = "";
    private final static String output_topic = "";

    public static void main(String[] args) throws Exception {
        Properties props = new Properties();
        //TODO: set properties

        final StreamsBuilder builder = new StreamsBuilder();

        //TODO "subscribe" to input topic
        KStream<String, String> source = null;

        //TODO filter and write to output topic

        //TODO create kafka streams object
        final KafkaStreams streams = null;
        final CountDownLatch latch = new CountDownLatch(1);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            //TODO: start stream

            latch.await();
        } catch (Throwable e) {
            System.exit(1);
        }
        System.exit(0);
    }

}
