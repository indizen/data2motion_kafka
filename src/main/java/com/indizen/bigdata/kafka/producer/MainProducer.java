package com.indizen.bigdata.kafka.producer;


import static com.indizen.bigdata.kafka.producer.MyProducer.runProducer;

public class MainProducer {

    public static void main(String... args) throws Exception {
        runProducer(50);

        // runProducerPartitioned(50);

        // runProducerEmp(50);
    }
}
