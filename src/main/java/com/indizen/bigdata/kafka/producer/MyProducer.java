package com.indizen.bigdata.kafka.producer;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class MyProducer {

    private final static String TOPIC = "";
    private final static String TOPIC_PARTITIONED = "";
    private final static String BOOTSTRAP_SERVERS = "";

    private static Producer<Integer, String> createProducer() {
        Properties props = new Properties();

        //TODO create producer
        return null;
    }

    private static Producer<Integer, String> createProducerWPartitioner() {
        Properties props = new Properties();

        //TODO create producer
        return null;
    }

    static void runProducer(int desiredMessages) throws Exception {
        final Producer<Integer, String> producer = createProducer();
        long time = System.currentTimeMillis();

        try {
            for (int index = 1; index < desiredMessages; index++) {
                //TODO: create record
                ProducerRecord<Integer, String> record = null;

                //TODO: send record
                RecordMetadata metadata = null;

                long elapsedTime = System.currentTimeMillis() - time;
                System.out.printf("sent record(key=%s value=%s) " + "meta(partition=%d, offset=%d) time=%d\n", "", "", "", "", "");

            }
        } finally {
            //TODO: close producer
        }
    }

    static void runProducerPartitioned(int desiredMessages) throws Exception {
        final Producer<Integer, String> producer = createProducer();
        long time = System.currentTimeMillis();

        try {
            for (int index = 1; index < desiredMessages; index++) {
                //TODO: create record
                ProducerRecord<Integer, String> record = null;

                //TODO: send record
                RecordMetadata metadata = null;

                long elapsedTime = System.currentTimeMillis() - time;
                System.out.printf("sent record(key=%s value=%s) " + "meta(partition=%d, offset=%d) time=%d\n", "", "", "", "", "");
            }
        } finally {
            //TODO: close producer
        }
    }
}
