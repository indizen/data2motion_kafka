package com.indizen.bigdata.kafka.producer.emp;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.IntegerSerializer;

import java.util.Properties;

public class KafKaProducerEmp {

    private final static String TOPIC = "test";
    private final static String BOOTSTRAP_SERVERS = "localhost:9092";

    private static Producer<Integer, com.indizen.bigdata.kafka.producer.emp.Employee> createProducer() {
        Properties props = new Properties();

        //TODO create producer
        return null;
    }

    public static void runProducerEmp(int desiredMessages) throws Exception {
        final Producer<Integer, com.indizen.bigdata.kafka.producer.emp.Employee> producer = createProducer();
        long time = System.currentTimeMillis();

        try {
            for (int index = 1; index < desiredMessages; index++) {
                // TODO: create record
                ProducerRecord<Integer, com.indizen.bigdata.kafka.producer.emp.Employee> record = null;

                //TODO: send record
                RecordMetadata metadata = null;

                long elapsedTime = System.currentTimeMillis() - time;
                System.out.printf("sent record(key=%s value=%s) " + "meta(partition=%d, offset=%d) time=%d\n", "", "", "", "", "");
            }
        } finally {
            //TODO: close producer
        }
    }
}
